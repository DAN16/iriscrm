<?php

namespace App\Signature;

interface SignatureCalculatorInterface
{
    public function verify(array $data, string $signature): bool;
}