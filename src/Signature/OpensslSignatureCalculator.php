<?php

declare(strict_types=1);

namespace App\Signature;

class OpensslSignatureCalculator implements SignatureCalculatorInterface
{
    private string $publicKey;

    public function __construct(string $publicKey)
    {
        $this->publicKey = $publicKey;
    }

    public function verify(array $data, string $signature): bool
    {
        $publicKey = openssl_get_publickey(trim($this->publicKey));
        $plainText = $this->serializeData($data);

        return 1 === openssl_verify(
                $plainText,
                base64_decode($signature),
                $publicKey,
                OPENSSL_ALGO_SHA1
            );
    }

    private function serializeData(array $data): string
    {
        unset($data['p_signature']);

        ksort($data);
        foreach ($data as $k => $v) {
            if (in_array(gettype($v), ['object', 'array']) === true) {
                continue;
            }

            $data[$k] = (string)$v;
        }
        return serialize($data);
    }
}
