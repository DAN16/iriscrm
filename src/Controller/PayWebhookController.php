<?php

declare(strict_types=1);

namespace App\Controller;

use App\Signature\SignatureCalculatorInterface;
use App\Storage\PayStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayWebhookController
{
    private const FIELD_ALERT_NAME = 'alert_name';

    private const FIELD_EMAIL = 'email';

    private const ALERT_NAME_SUBSCRIPTION_CREATED = 'subscription_created';

    private const FIELD_STATUS = 'status';

    private PayStorageInterface $payStorage;

    private LoggerInterface $logger;

    private SignatureCalculatorInterface $signatureCalculator;

    public function __construct(PayStorageInterface $payStorage, LoggerInterface $logger, SignatureCalculatorInterface $signatureCalculator)
    {
        $this->payStorage = $payStorage;
        $this->logger = $logger;
        $this->signatureCalculator = $signatureCalculator;
    }

    public function __invoke(Request $request): Response
    {
        $isVerify = $this->signatureCalculator->verify(
            $request->request->all(),
            $request->request->get('p_signature')
        );
        if ($isVerify === false) {
            return new JsonResponse((object)[], 403);
        }
        $this->logger->debug('request', $request->request->all());

        $alertName = $request->request->get(self::FIELD_ALERT_NAME, '');
        $email = $request->request->get(self::FIELD_EMAIL, '');

        if (!in_array($alertName, [self::ALERT_NAME_SUBSCRIPTION_CREATED])) {
            return new JsonResponse((object)[], 403);
        }

        if ($this->payStorage->saveUser($email, $request->request->get(self::FIELD_STATUS, '')) === false) {
            return new JsonResponse((object)[], 403);
        }

        return new JsonResponse([]);
    }
}
