<?php

declare(strict_types=1);

namespace App\Controller;

use App\Pay\Client\Api\PayApiClientInterface;
use App\Storage\PayStorageInterface;
use Predis\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayUpdatePlanController
{
    private PayStorageInterface $payStorage;

    private PayApiClientInterface $apiClient;

    public function __construct(PayStorageInterface $payStorage, PayApiClientInterface $apiClient)
    {
        $this->payStorage = $payStorage;
        $this->apiClient = $apiClient;
    }

    public function __invoke(Request $request): Response
    {
        $email = $request->request->get('email');
        if ($email === null) {
            return new JsonResponse((object)[]);
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return new JsonResponse((object)[]);
        }

        $data = $this->payStorage->isEmailExists($email);
        if ($data === false) {
            return new JsonResponse((object)[]);
        }

        return new JsonResponse([
            'status' => $this->$apiClient->updateUser($email)
        ]);
    }
}
