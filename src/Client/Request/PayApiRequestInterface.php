<?php

declare(strict_types=1);

namespace App\Pay\Client\Request;

interface PayApiRequestInterface
{
    public function getVendorId(): int;

    public function getVendorAuthCode(): string;

    public function getAnnualPlanId(): int;

    public function getUsersUpdatePath(): string;
}