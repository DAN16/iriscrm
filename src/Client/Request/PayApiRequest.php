<?php

declare(strict_types=1);

namespace App\Pay\Client\Request;

class PayApiRequest implements PayApiRequestInterface
{
    private int $vendorId;

    private string $vendorAuthCode;

    private int $annualPlanId;

    private string $usersUpdatePath;

    public function __construct(int $vendorId, string $vendorAuthCode, int $annualPlanId, string $usersUpdatePath)
    {
        $this->vendorId = $vendorId;
        $this->vendorAuthCode = $vendorAuthCode;
        $this->annualPlanId = $annualPlanId;
        $this->usersUpdatePath = $usersUpdatePath;
    }

    public function getVendorId(): int
    {
        return $this->vendorId;
    }

    public function getVendorAuthCode(): string
    {
        return $this->vendorAuthCode;
    }

    public function getAnnualPlanId(): int
    {
        return $this->annualPlanId;
    }

    public function getUsersUpdatePath(): string
    {
        return $this->usersUpdatePath;
    }
}