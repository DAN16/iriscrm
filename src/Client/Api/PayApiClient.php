<?php

declare(strict_types=1);

namespace App\Pay\Client\Api;

use App\Pay\Client\Request\PayApiRequestInterface;
use Http\Client\Curl\CurlClientInterface;
use Http\Client\Curl\Request\Builder\RequestBuilder;
use Symfony\Component\HttpFoundation\Response;

class PayApiClient implements PayApiClientInterface
{
    private PayApiRequestInterface $request;

    private RequestBuilder $requestBuilder;

    private CurlClientInterface $curlClient;

    private int $timeout;

    private int $connectionTimeout;

    public function __construct(
        PayApiRequestInterface $request,
        RequestBuilder         $requestBuilder,
        CurlClientInterface    $curlClient,
        int                    $timeout,
        int                    $connectionTimeout
    )
    {
        $this->request = $request;
        $this->requestBuilder = $requestBuilder;
        $this->curlClient = $curlClient;
        $this->timeout = $timeout;
        $this->connectionTimeout = $connectionTimeout;
    }

    public function updateUser(string $email): bool
    {
        $request = (clone $this->requestBuilder)
            ->uri($this->request->getUsersUpdatePath())
            ->contentType('application/x-www-form-urlencoded')
            ->post()
            ->body([
                'vendor_id' => $this->request->getVendorId(),
                'vendor_auth_code' => $this->request->getVendorAuthCode(),
                'email' => $email,
                'quantity' => 1,
                'bill_immediately' => true,
                'plan_id' => $this->request->getAnnualPlanId(),
            ])
            ->timeout($this->timeout)
            ->connect($this->connectionTimeout)
            ->build();

        $response = $this->curlClient->send($request);

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            return false;
        }

        $decoded = json_decode((string)$response->getBody(), true);

        if ($decoded === null) {
            return false;
        }

        return (bool)$decoded['success'];
    }
}