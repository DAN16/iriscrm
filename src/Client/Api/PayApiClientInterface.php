<?php

declare(strict_types=1);

namespace App\Pay\Client\Api;

interface PayApiClientInterface
{
    public function updateUser(string $email): bool;
}