<?php

namespace App\Storage;

use Predis\Client;

class PayStorage implements PayStorageInterface
{
    private const FIELD_STATUS = 'status';

    private const FIELD_EMAIL = 'email';

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function saveUser(string $email, string $status): bool
    {
        return $this->client->hmset(
            'user:' . $email,
            [
                self::FIELD_STATUS => $status,
                self::FIELD_EMAIL => $email
            ]
        );
    }

    public function isEmailExists(string $email): bool
    {
        return (bool)$this->client->hexists('user:' . $email);
    }
}