<?php

namespace App\Storage;

interface PayStorageInterface
{
    public function saveUser(string $email, string $status): bool;

    public function isEmailExists(string $email): bool;
}